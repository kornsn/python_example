import socket
from logging import getLogger
from pickle import loads, dumps
from threading import Thread

from amqp import Connection, Message

from messaging import IMessenger

logger = getLogger(__name__)


class AMQPMessenger(IMessenger):
    """
    Implementation of ``IMessenger`` based on simple ``amqp`` library.
    """

    def __init__(self, connection_conf, exchange, version=None):
        """
        Args:
            connection_conf (dict): Connection config. Will be passed to ``Connection`` as **connection_conf.
            exchange (str): Exchange name.
            version (str): Messenger version. Need to use several instances of backend with the same broker.
        """
        connection = Connection(**connection_conf)
        channel = connection.channel()
        channel.exchange_declare(exchange, type='direct', durable='True', auto_delete=False)
        self.__connection = connection
        self.__channel = channel
        self.__exchange = exchange
        self.__version = version
        self.__should_stop = False
        self.__messages = dict()
        self.__thread = None

    def ensure_queue(self, address, persistent=False):
        """
        Ensures that queue is exists.

        Args:
            address (str): Address to bind queue.
            persistent (bool): Indicates if queue must be persistent.
        """
        address = self.__versioned(address)
        if persistent:
            self.__channel.queue_declare(queue=address, durable=True, auto_delete=False)
        else:
            self.__channel.queue_declare(queue=address, durable=False, auto_delete=True)
        self.__channel.queue_bind(queue=address, exchange=self.__exchange, routing_key=address)
        logger.debug('Queue %s ensured. Persistent: %s', address, persistent)

    def register(self, address, callback):
        """
        Registers specified address for receiving messages.
        When message will be received, ``callback`` will be called.
        Must be called before messenger starts.

        Args:
            address (str): Address for message receiving.
            callback (callable): Callback for processing message.
                Signature of callback: (ctx: dict, body: dict).
        """
        address = self.__versioned(address)
        c = _CallbackWrapper(callback, self.__messages)
        self.__channel.basic_consume(queue=address, callback=c)
        logger.debug('Address %s registered. Callback: %s', address, callback)

    def start(self):
        """
        Starts messenger.
        """
        thread = Thread(target=self.__run)
        self.__thread = thread
        thread.start()
        logger.info('Messenger started.')

    def __run(self):
        connection = self.__connection
        while not self.__should_stop:
            try:
                connection.drain_events(0.2)
            except socket.timeout:
                pass
        connection.close()
        logger.info('Messenger stopped')

    def send(self, ctx, body, route):
        """
        Sends message to specified route.

        Args:
            ctx (dict): Message context.
            body (dict): Message body
            route (str): Route.
        """
        route = self.__versioned(route)
        self.__channel.basic_publish(msg=Message(body=dumps(body), **ctx),
                                     exchange=self.__exchange,
                                     routing_key=route)
        logger.debug('Message sent. Context: %s, Body: %s, Route: %s', ctx, body, route)

    def ack(self, ctx):
        """
        Acknowledge message by ctx.

        Args:
            ctx (dict): Message context.
        """
        delivery_tag = ctx['delivery_tag']
        messages = self.__messages
        message = messages[delivery_tag]
        del messages[delivery_tag]
        message.channel.basic_ack(delivery_tag)
        logger.debug('Message %s acknowledged.', delivery_tag)

    def should_stop(self):
        """
        Stop messenger asynchronously.
        """
        logger.info("Messenger should be stopped.")
        self.__should_stop = True

    def join(self):
        """
        Join while messenger will stopped.
        """
        self.__thread.join()

    def __versioned(self, s):
        if self.__version:
            return self.__version + '___' + s
        else:
            return s


class _CallbackWrapper(object):
    """
    Inner class for pre-processing received messages.
    """

    def __init__(self, callback, messages):
        """
        Args:
            callback (callable): Callback to call on message received.
            messages (dict): Dict of messages in form of {delivery_tag: message}
                to store received messages for further acknowledgement.
        """
        self.__callback = callback
        self.__messages = messages

    def __call__(self, message):
        """
        Args:
            message (Message): Message
        """
        delivery_tag = message.delivery_tag
        self.__messages[delivery_tag] = message
        body = loads(message.body)
        ctx = message.properties
        ctx['delivery_tag'] = delivery_tag
        logger.debug('Message got. Body: %s, Context: %s', body, ctx)
        self.__callback(ctx, body)
