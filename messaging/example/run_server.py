from concurrent.futures import ThreadPoolExecutor
from logging import basicConfig, NOTSET
from time import sleep

from messaging.example.client_server import ExampleServer
from messaging.kombu_messenger import KombuMessenger

basicConfig(level=NOTSET, format='%(asctime)s: %(levelname)s: %(name)s: %(message)s')

messenger = KombuMessenger(connection_conf={'hostname': 'amqp://localhost//'},
                           exchange='test',
                           version='ascvvdv')
server = ExampleServer(messenger, 'server')
server.register()
messenger.start()
with ThreadPoolExecutor(max_workers=10) as pool:
    server.start_with_executor(pool)
    sleep(10)
    print('Messenger should stop')
    server.should_stop()
    server.join()
    messenger.should_stop()
