from logging import getLogger

from messaging import MessagingError, ClientBase, ServerBase

logger = getLogger(__name__)

HELLO = 'hello'


class ExampleClient(ClientBase):
    """
    Example implementation of ``ClientBase``.
    """

    def hello(self, greeting):
        """
        Send command "HELLO" synchronously.

        Args:
            greeting (str): Greeting.

        Returns:
            ctx, Server response.
        """
        return self.rpc(HELLO, greeting=greeting)

    def hello_async(self, greeting, callback):
        """
        Send command "HELLO" asynchronously.

        Args:
            greeting (str): Greeting.
            callback (callable): Callable with signature (ctx: dict, status: str) to catch result.
        """
        return self.send_with_reply(HELLO, callback, greeting=greeting)

    def unknown_command(self, args, callback):
        """
        Sends command that unknown for server. Do not do this in real world.

        Args:
            args: Some args.
            callback: Callback.
        """
        return self.send_with_reply('unknown', callback, args=args)

    def process_message(self, ctx, body):
        """
        Just add error handling to super().process_message.

        Args:
            ctx (dict): Context of message.
            body (dict): Body of message.
        """
        try:
            super().process_message(ctx, body)
        except MessagingError:
            logger.exception("Error while processing message.")


class ExampleServer(ServerBase):
    """
    Example implementation of ``ServerBase``.
    """

    def get_commands_mapping(self):
        """
        Commands mapping.
        """
        return {HELLO: self.__on_hello}

    def __on_hello(self, ctx, greeting):
        """
        `HELLO` command handler.

        Args:
            ctx (dict): Message context.
            greeting (str): Greeting.
        """
        print('Server got command Hello,', greeting, ctx)
        self.reply_to(ctx, status='ok')

    def process_message(self, ctx, body):
        """
        Just add error handling to super().process_message.

        Args:
            ctx (dict): Message context.
            body (dict): Message body.
        """
        try:
            super().process_message(ctx, body)
        except MessagingError:
            logger.exception("Error while processing message.")
