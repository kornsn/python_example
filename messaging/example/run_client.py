from concurrent.futures import ThreadPoolExecutor
from logging import basicConfig, NOTSET
from time import sleep

from messaging.example.client_server import ExampleClient
from messaging.kombu_messenger import KombuMessenger

basicConfig(level=NOTSET, format='%(asctime)s: %(levelname)s: %(name)s: %(message)s')

messenger = KombuMessenger(connection_conf={'hostname': 'amqp://localhost//'},
                           exchange='test',
                           version='ascvvdv')
client = ExampleClient(messenger, 'server', debug=True)
client.register()
messenger.start()
with ThreadPoolExecutor(max_workers=10) as pool:
    client.start_with_executor(pool)

    print('--------\nTest RPC')
    ctx, result = client.hello('world!')
    print('Result Context:', ctx)
    print('Result Answer:', result)

    print('--------\nTest async')
    client.hello_async('World async!',
                       lambda ctx0, status: print('Async: Context: %s; Status: %s' % (ctx0, status)))

    print('--------\nTest Unknown')
    client.unknown_command('zxc', print)

    print('--------\nTest RPC again')
    ctx, result = client.hello('world!')
    print('Result Context:', ctx)
    print('Result Answer:', result)

    sleep(10)
    print('Messenger should stop')
    client.should_stop()
    client.join()
    messenger.should_stop()
