from logging import getLogger
from threading import Thread

from kombu import Connection, producers, Exchange, Queue
from kombu.mixins import ConsumerMixin

from messaging import IMessenger

logger = getLogger(__name__)


class KombuMessenger(IMessenger):
    """
    Implementation of ``IMessenger`` based on ``kombu`` library.
    """
    def __init__(self, connection_conf, exchange, version=None):
        """
        Args:
            connection_conf (dict): Connection config. Will be passed to ``Connection`` as **connection_conf.
            exchange (str): Exchange name.
            version (str): Messenger version. Need to use several instances of backend with the same broker.
        """
        connection = Connection(**connection_conf)
        channel = connection.channel()
        exchange = Exchange(exchange, type='direct', durable='True', auto_delete=False, channel=channel)
        exchange.declare()
        self.__connection = connection
        self.__channel = channel
        self.__exchange = exchange
        self.__version = version
        self.__worker = None
        self.__queues = dict()  # {address: queue}
        self.__callbacks = dict()  # {address: callback}
        self.__thread = None
        self.__messages = dict()

    def ensure_queue(self, address, persistent=False):
        """
        Ensures that queue is exists.

        Args:
            address (str): Address to bind queue.
            persistent (bool): Indicates if queue must be persistent.
        """
        address = self.__versioned(address)
        queue = Queue(address, self.__exchange, address, self.__channel, durable=persistent)
        queue.declare()
        self.__queues[address] = queue
        logger.debug('Queue %s ensured. Persistent: %s', address, persistent)

    def register(self, address, callback):
        """
        Registers specified address for receiving messages.
        When message will be received, ``callback`` will be called.
        Must be called before messenger starts.

        Args:
            address (str): Address for message receiving.
            callback (callable): Callback for processing message.
                Signature of callback: (ctx: dict, body: dict).
        """
        callback = _CallbackWrapper(callback, self.__messages)
        address = self.__versioned(address)
        self.__callbacks[address] = callback
        logger.debug('Address %s registered. Callback: %s', address, callback)

    def start(self):
        """
        Starts messenger.
        """
        thread = Thread(target=self.__run)
        thread.start()
        self.__thread = thread
        logger.info('Messenger started.')

    def __run(self):
        worker = _Worker(self.__connection, self.__queues, self.__callbacks)
        self.__worker = worker
        worker.run()
        self.__connection.close()
        logger.info('Messenger stopped.')

    def send(self, ctx, body, route):
        """
        Sends message to specified route.

        Args:
            ctx (dict): Message context.
            body (dict): Message body
            route (str): Route.
        """
        route = self.__versioned(route)
        with producers[self.__connection].acquire(block=True) as producer:
            producer.publish(body,
                             headers=ctx,
                             serializer='pickle',
                             exchange=self.__exchange,
                             no_ack=True,
                             routing_key=route)
        logger.debug('Message sent. Context: %s, Body: %s, Route: %s', ctx, body, route)

    def ack(self, ctx):
        """
        Acknowledge message by ctx.

        Args:
            ctx (dict): Message context.
        """
        delivery_tag = ctx['delivery_tag']
        messages = self.__messages
        message = messages[delivery_tag]
        del messages[delivery_tag]
        message.ack()
        logger.debug('Message %s acknowledged.', delivery_tag)

    def should_stop(self):
        """
        Stop messenger asynchronously.
        """
        logger.info("Messenger should be stopped.")
        self.__worker.should_stop = True

    def join(self):
        """
        Join while messenger will stopped.
        """
        self.__thread.join()
        pass

    def __versioned(self, s):
        if self.__version:
            return self.__version + '___' + s
        else:
            return s


class _Worker(ConsumerMixin):
    """
    Inner class for message consuming.
    """
    def __init__(self, connection, queues, callbacks):
        self.connection = connection
        self.__queues = queues
        self.__callbacks = callbacks

    def get_consumers(self, consumer_builder, channel):
        return [consumer_builder(queues=[queue],
                                 accept=['pickle'],
                                 callbacks=[callback])
                for k, queue in self.__queues.items()
                for k1, callback in self.__callbacks.items()
                if k == k1
                ]


class _CallbackWrapper(object):
    """
    Inner class for pre-processing received messages.
    """

    def __init__(self, callback, messages):
        """
        Args:
            callback (callable): Callback to call on message received.
            messages (dict): Dict of messages in form of {delivery_tag: message}
                to store received messages for further acknowledgement.
        """
        self.__callback = callback
        self.__messages = messages

    def __call__(self, body, message):
        delivery_tag = message.delivery_tag
        self.__messages[delivery_tag] = message
        ctx = message.headers
        ctx['delivery_tag'] = delivery_tag
        logger.debug('Message got. Body: %s, Context: %s', body, ctx)
        self.__callback(ctx, body)
