from abc import ABCMeta, abstractmethod
from concurrent.futures import Future
from logging import getLogger
from queue import Queue
from threading import Lock, Thread
from uuid import uuid4

logger = getLogger(__name__)


def new_cid():
    """
    Generate new unique correlation id.

    Returns:
        str: Unique correlation id.
    """
    return uuid4().hex


class IMessenger(metaclass=ABCMeta):
    """
    Interface for messenger.
    """

    @abstractmethod
    def ensure_queue(self, address, persistent=False):
        """
        Ensures that queue is exists.

        Args:
            address (str): Address to bind queue.
            persistent (bool): Indicates if queue must be persistent.
        """
        pass

    @abstractmethod
    def register(self, address, callback):
        """
        Registers specified address for receiving messages.
        When message will be received, ``callback`` will be called.
        Must be called before messenger starts.

        Args:
            address (str): Address for message receiving.
            callback (callable): Callback for processing message.
                Signature of callback: (ctx: dict, body: dict).
        """
        pass

    @abstractmethod
    def start(self):
        """
        Starts messenger.
        """
        pass

    @abstractmethod
    def send(self, ctx, body, route):
        """
        Sends message to specified route.

        Args:
            ctx (dict): Message context.
            body (dict): Message body
            route (str): Route.
        """
        pass

    @abstractmethod
    def ack(self, ctx):
        """
        Acknowledge message by ctx.

        Args:
            ctx (dict): Message context.
        """
        pass

    @abstractmethod
    def should_stop(self):
        """
        Stop messenger asynchronously.
        """
        pass

    @abstractmethod
    def join(self):
        """
        Join while messenger will stopped.
        """
        pass


class ConsumerBase(object):
    """
    Interface of message consumer.

    Attributes:
        address (str): Address for consuming messages.
        messenger (IMessenger): Instance of messenger.
    """

    def __init__(self, messenger, address):
        """
        Args:
            messenger (IMessenger): Instance of messenger.
            address (str): Address for consuming messages.
        """
        self.messenger = messenger
        self.address = address
        self.__queue = Queue()
        self.__stop = False

    def register(self):
        """
        Register this instance for receiving messages from messenger.
        """
        self.messenger.register(self.address, self.__on_message_received)

    def __on_message_received(self, ctx, body):
        """
        This method is called when new message received.

        Args:
            ctx (dict): Context of message.
            body (dict): Body of message.
        """
        if not self.__stop:
            self.__queue.put((ctx, body))

    def start_with_executor(self, executor):
        """
        Starts consuming with ``executor`` as executor for message processing.

        Args:
            executor: Instance of ThreadPoolExecutor.
        """
        Thread(target=self.__run, args=(executor,), daemon=True).start()

    def __run(self, executor):
        fn = self.process_message
        queue = self.__queue
        while not self.__stop:
            ctx, body = queue.get()
            if ctx is None:
                queue.task_done()
                break
            f = executor.submit(fn, ctx, body)
            queue.task_done()
            f.add_done_callback(_raise_exception)
        logger.info('Consuming at %s stopped.', self.address)

    def should_stop(self):
        """
        Stops consuming asynchronously.
        """
        logger.info("Consuming at %s should be stopped.", self.address)
        self.__stop = True
        self.__queue.put((None, None))

    def join(self):
        """
        Join while consuming will be stopped.
        """
        self.__queue.join()

    def process_message(self, ctx, body):
        """
        Abstract method for processing messages.

        Args:
            ctx (dict): Context of messages.
            body (dict): Body of message.
        """
        raise NotImplemented

    def ack(self, ctx):
        """
        Acknowledge message.

        Args:
            ctx (dict): Message context.
        """
        self.messenger.ack(ctx)


class ClientBase(ConsumerBase):
    """
    Base implementation of client.
    """

    def __init__(self, messenger, server_address, address=None, debug=True):
        """
        Args:
            messenger (IMessenger): Instance of messenger.
            server_address (str): Address of server which client talk to.
            address (str): Address of this client. If one is not specified, new unique address will be generated.
            debug (bool): Indicates if it is needed to run client in debug mode.
                In debug mode client does not pass messages with unknown correlation ids.
        """
        super().__init__(messenger, address or new_cid())
        self.server_address = server_address
        self.debug = debug
        self.__callbacks = dict()

        messenger.ensure_queue(self.address, persistent=False)
        messenger.ensure_queue(self.server_address, persistent=True)

    def send(self, command, cid=None, **kwargs):
        """
        Sends message.

        Args:
            command (str): Command for sending to server.
            cid (str): Correlation id of message.
            **kwargs: Arguments of command.
        """
        ctx_builder = _ContextBuilder()
        if cid is not None:
            ctx_builder.set_cid(cid)
        ctx = ctx_builder.build()
        body = {'command': command, 'kwargs': kwargs}
        self.messenger.send(ctx, body, self.server_address)

    def send_with_reply(self, command, callback, cid=None, **kwargs):
        """
        Sends message and expect answer.

        Args:
            command (str): Command for sending to server.
            callback (callable): Method that will be called on message received.
                Signature of callable: (body: dict, message: Message)
            cid: Correlation id of message.
            **kwargs: Arguments of command.
        """
        cid = cid or new_cid()
        ctx = _ContextBuilder().set_cid(cid).set_reply_to(self.address).build()
        body = {'command': command, 'kwargs': kwargs}
        self.__callbacks[cid] = callback
        self.messenger.send(ctx, body, self.server_address)

    def rpc(self, command, timeout=None, cid=None, **kwargs):
        """
        Remote procedure call.

        Args:
            command (str): Command for sending to server.
            timeout (float): The number of seconds to wait for the result if the future
                isn't done. If None, then there is no limit on the wait time.
            cid: Correlation id of message.
            **kwargs: Arguments of command.

        Returns:
            dict, dict: context, server_answer

        Raises:
            TimeoutError: If the remote procedure call didn't finish executing before the given
                timeout.
        """
        callback = _RPCWrapper()
        cid = cid or new_cid()
        ctx = _ContextBuilder().set_cid(cid).set_reply_to(self.address).build()
        body = {'command': command, 'kwargs': kwargs}
        self.__callbacks[cid] = callback
        self.messenger.send(ctx, body, self.server_address)
        return callback.result(timeout)

    def process_message(self, ctx, body):
        """
        Method for processing messages.

        Args:
            ctx (dict): Context of message.
            body (dict): Body of message.
        """
        # Acknowledge just at received.
        self.ack(ctx)

        cid = ctx['correlation_id']
        callbacks = self.__callbacks
        if not self.debug:
            # process only existing correlation ids
            if cid in callbacks:
                callback = callbacks[cid]
                del callbacks[cid]
                callback(ctx, **body)
        else:
            # In debug mode process all correlation ids.
            # Sometimes messages can come twice, so KeyError can be raised.
            callback = callbacks[cid]
            del callbacks[cid]
            callback(ctx, **body)


class ServerBase(ConsumerBase, metaclass=ABCMeta):
    """
    Base implementation of server.
    """
    def __init__(self, messenger, address):
        """
        Args:
            messenger (IMessenger): Instance of messenger.
        """
        super().__init__(messenger, address)

        messenger.ensure_queue(address, persistent=True)

    def process_message(self, ctx, body):
        """
        Method for processing messages.

        Args:
            ctx (dict): Message context.
            body (dict): Message body.
        """
        self.ack(ctx)

        command = body['command']
        commands_mapping = self.get_commands_mapping()
        if command in commands_mapping:
            logger.debug('Processing message. Context: %s, Body: %s', ctx, body)
            commands_mapping[command](ctx, **body['kwargs'])
        else:
            raise UnknownCommandError('Unknown command "%s"' % command)

    @abstractmethod
    def get_commands_mapping(self):
        """
        Returns:
            dict[str, callable]: Map of commands and callbacks
                in form of dict[command_name: str, callback: callable].
        """
        # It's just example.
        # Override it in your own implementation.
        return {'example_command': self.__on_example_command,
                'command_with_reply': self.__on_command_with_reply}

    def reply_to(self, ctx, **kwargs):
        """
        Replies to message.

        Args:
            ctx (dict): Context of message.
            **kwargs: Arguments of message.
        """
        my_ctx = _ContextBuilder().set_cid(ctx['correlation_id']).build()
        self.messenger.send(my_ctx, kwargs, ctx['reply_to'])

    def __on_example_command(self, ctx, **kwargs):
        print(self, kwargs, ctx)

    def __on_command_with_reply(self, ctx, **kwargs):
        print(self, kwargs, ctx)
        self.reply_to(ctx, answer='Hi there!', status='alive')


def _raise_exception(future):
    """
    Raises exception raised in ThreadPoolExecutor.

    Args:
        future (Future): Instance of future.
    """

    exc = future.exception()
    if exc:
        raise exc


class _ContextBuilder(object):
    """
    Inner class for building messages.
    """

    def __init__(self):
        self.__source = {}

    def set_reply_to(self, address):
        self.__source['reply_to'] = address
        return self

    def set_cid(self, cid):
        self.__source['correlation_id'] = cid
        return self

    def build(self):
        return self.__source


class _RPCWrapper(object):
    """
    Inner class for getting result of remote call procedure.
    """

    def __init__(self):
        self.__future = Future()

    def __call__(self, ctx, **kwargs):
        self.__future.set_result((ctx, kwargs))

    def result(self, timeout=None):
        result = self.__future.result(timeout)
        return result


class MessagingError(Exception):
    """
    Base class for errors occurred in messaging.
    """
    pass


class UnknownCommandError(MessagingError):
    """
    Raised when server got unknown command.
    """
    pass


class DummyMessenger(IMessenger):
    """
    Simple dummy in-memory messenger. Works in single process only.
    Do not store messages and does not support message acknowledgement.
    """

    __queues = dict()
    __queues_lock = Lock()

    def ensure_queue(self, address, persistent=False):
        """
        Ensures that queue is exists.

        Args:
            address (str): Address to bind queue.
            persistent (bool): Indicates if queue must be persistent.
        """
        queues = self.__queues
        with self.__queues_lock:
            if address not in queues:
                queues[address] = []

    def register(self, address, callback):
        """
        Registers specified address for receiving messages.
        When message will be received, ``callback`` will be called.
        Must be called before messenger starts.

        Args:
            address (str): Address for message receiving.
            callback (callable): Callback for processing message.
                Signature of callback: (ctx: dict, body: dict).
        """
        self.__queues[address].append(callback)

    def start(self):
        """
        Starts messenger.
        """
        pass

    def send(self, ctx, body, route):
        """
        Sends message to specified route.

        Args:
            ctx (dict): Message context.
            body (dict): Message body
            route (str): Route.
        """
        callbacks = self.__queues[route]
        for callback in callbacks:
            callback(ctx, body)

    def ack(self, ctx):
        """
        Acknowledge message by ctx.

        Args:
            ctx (dict): Message context.
        """
        pass

    def should_stop(self):
        """
        Stop messenger asynchronously.
        """
        pass

    def join(self):
        """
        Join while messenger will stopped.
        """
        pass
